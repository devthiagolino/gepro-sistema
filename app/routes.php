<?php

//pra tudo tem que está autenticado
Route::group(array('before' => 'auth_sentry'), function() {

  Route::get('/', array(
      'as' => 'dashboard',
      'uses' => 'Admin\UsersController@index'
          )
  );
//Rota de Clientes
  Route::controller('empresas', 'Admin\ClientesController');
});

//rota de login
Route::get('login', array("as" => "login", function() {
return View::make("login");
}));

Route::post('user/autenticar', array("as" => "postar_login", "uses" => "Admin\UsersController@login"));
Route::get('user/logout', array("as" => "logout", "uses" => "Admin\UsersController@logout"));
Route::post('user/store', "Admin\UsersController@admin/store");
