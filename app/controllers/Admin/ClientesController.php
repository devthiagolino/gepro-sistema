<?php

namespace Admin;

use \View,
    \Sentry,
    \Redirect,
    \Input,
    \Session,
    \Validator,
    \Cartalyst\Sentry\Users;

class ClientesController extends \BaseController {

  public function getIndex() {
    $clientes = Cliente::paginate(10);
    $this->layout->content = View::make('admin.clientes.index', array('clientes' => $clientes));
  }

  public function getCreate() {
    $this->layout->content = View::make('admin.clientes.create');
  }

  public function postStore() {

    $cliente = new Cliente;    
    $data = Input::all();
    unset($data['_token']);
    $v = Validator::make(Input::all(), $cliente::$rules);
    if ($v->fails()) {
      return Redirect::back()->withErrors($v)->withInput();
    }

    $replaces = array(
        'celular_responsavel',
        'telefone_responsavel',
        'cnpj',
        'inscricao_estadual',
        'inscricao_municipal',
        'reg_mec',
        'telefone_empresarial',
        'cpf_responsavel'
    );

    foreach ($replaces as $r) {
      $data[$r] = str_replace(array(' ', '/', '-', '(', ')', '.'), '', $data[$r]);
    }
    
    foreach ($data as $key => $d) {
      $cliente->$key = $data[$key];
    }

    if (!array_key_exists('status', $data)) {
      $cliente->status = 0;
    }
    
    $cliente->save();
    
    if (self::existeFoto(Input::file('imagem'))) {
      self::uploadImagem(Input::file('imagem'), 'uploads', $cliente->id . '_empresa.' . Input::file(Input::file('imagem'))->getClientOriginalExtension());
    }
    
    return Redirect::to('/clientes')->with('notice', "Cliente [\$data['razao_social']\] foi inserido com sucesso.");
  }

  public function getShow($id) {
    $cliente = Cliente::findOrFail($id);
    $this->layout->content = View::make('admin.clientes.show', array("cliente" => $cliente));
  }

  public function edit($id) {
    $cliente = Cliente::findOrFail($id);
    return $this->layout->content = View::make('admin.clientes.create');
  }

  public function postUpdate($id) {

    $data = Input::all();
    unset($data['_token']);
    $v = Validator::make(Input::all(), Cliente::$rules);
    if ($v->fails()) {
      return Redirect::back()->withErrors($v)->withInput();
    }

    $replaces = array(
        'celular_responsavel',
        'telefone_responsavel',
        'cnpj',
        'inscricao_estadual',
        'inscricao_municipal',
        'reg_mec',
        'telefone_empresarial',
        'cpf_responsavel'
    );

    foreach ($replaces as $r) {
      $data[$r] = str_replace(array(' ', '/', '-', '(', ')', '.'), '', $data[$r]);
    }

    $cliente = Cliente::findOrfail($id);
    foreach ($data as $key => $d) {
      $cliente->$key = $data[$key];
    }

    if (!array_key_exists('status', $data)) {
      $cliente->status = 0;
    }

    
    
    $nome = $cliente->id.'_empresa.' . Input::file('imagem')->getClientOriginalExtension();    
    if (self::existeFoto('imagem')) {
      self::uploadImagem('imagem', 'clientes', $nome);
      $cliente->imagem = Input::file('imagem')->getClientOriginalExtension();
    }
    
    $cliente->save();
    return Redirect::to('/empresas')->with('notice', "Cliente [\$data['razao_social']\] foi atualizado com sucesso.");
    
  }

  public function postDestroy(array $id) {
    //
  }

  private static function uploadImagem($input, $dir, $name = false) {
    if (!$name) {
      $name = Input::file($input)->getClientOriginalName();
    }

    $_extPermitidas = array('jpg', 'gif', 'png', 'jpeg');
    $extOriginal = Input::file($input)->getClientOriginalExtension();
    
    if (!in_array($extOriginal, $_extPermitidas)) {      
      return Redirect::back()->with('error', 'Você não enviou uma foto por favor envie o arquivo correto.')->withInput(Input::except('password'));
    }
    
    Input::file($input)->move($dir, $name);
  }

  private static function imagemValida($input) {
    if (Input::file($input)->isValid()) {
      return true;
    }
    return false;
  }

  private static function existeFoto($input) {
    if (Input::hasFile($input)) {
      return true;
    }
    return false;
  }

}
