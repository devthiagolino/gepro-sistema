<?php

namespace Admin;

use \View,
    \Sentry,
    \Redirect,
    \Input,
    \Session,
    \Cartalyst\Sentry\Users;

class UsersController extends \BaseController {

  public function index() {
    $usuario = Sentry::getUser();
    return View::make("admin.home", array('usuario' => $usuario));
  }

  public function login() {

    try {
      $credenciais = array(
          'email' => Input::get("username"),
          'password' => Input::get("password"),
      );
      $usuario = Sentry::authenticate($credenciais, false);      
      return Redirect::route("dashboard");
    } catch (\Cartalyst\Sentry\Users\LoginRequiredException $e) {
      Session::flash("error", "O campo (Login) é obrigatório");
      return Redirect::route("login");
    } catch (\Cartalyst\Sentry\Users\PasswordRequiredException $e) {
      Session::flash("error", "O campo (Senha) é obrigatório");
      return Redirect::route("login");
    } catch (\Cartalyst\Sentry\Users\WrongPasswordException $e) {
      Session::flash("error", "Senha incorreta, tente novamente");
      return Redirect::route("login");
    } catch (\Cartalyst\Sentry\Users\UserNotFoundException $e) {
      Session::flash("error", "Esse usuário não existe");
      return Redirect::route("login");
    } catch (\Cartalyst\Sentry\Users\UserNotActivatedException $e) {
      Session::flash("error", "Esse usuário não está ativo");
      return Redirect::route("login");
    }
  }

  public function create() {
    return View::make("admin.users.create");
  }

  public function store() {
    try {
      $user = Sentry::createUser(array(
                  'first_name' => Input::get("nome"),
                  'last_name' => Input::get("sobrenome"),
                  'email' => Input::get("email"),
                  'password' => Input::get("password"),
                  'activated' => Input::get("status"),
      ));

      // verificando se o grupo existe
      $adminGroup = Sentry::findGroupById(1);
      // definindo o grupo do usuário
      $user->addGroup($adminGroup);
      // se criou
      return Redirect::route("dashboard");
    } catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
      Session::flash("error", "O campo (Login) é obrigatório");
      return Redirect::back()->withInput(Input::except('password'));
    } catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
      Session::flash("error", "O campo (Senha) é obrigatório");
      return Redirect::back()->withInput(Input::except('password'));
    } catch (Cartalyst\Sentry\Users\UserExistsException $e) {
      Session::flash("error", "Usuário com esse (Login) já existe");
      return Redirect::back()->withInput(Input::except('password'));
    } catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e) {
      Session::flash("error", "O grupo escolhido não existe");
      return Redirect::back()->withInput(Input::except('password'));
    }
  }

  public function logout() {
    Sentry::logout();
    return Redirect::route("login");
  }

  public function show($id) {
    //aqui ele mostra o registro individual
  }

  public function edit($id) {
    //aqui ele mostra o form de edição
    return View::make("users.create");
  }

  public function update($id) {
    //aqui ele faz o update
  }

  public function destroy($id) {
    //aqui ele remove
  }

}
