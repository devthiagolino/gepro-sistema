<!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="active">
              <a href="../">
                <i class="fa fa-dashboard"></i> 
                <span>Dashboard</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-th"></i> 
                <span>Clientes</span> 
                <i class="fa fa-angle-left pull-right"></i>
                <!-- <small class="badge pull-right bg-green">new</small>-->
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ url('/empresas') }}"><i class="fa fa-angle-double-right"></i> Gerenciar</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Relatório</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-bar-chart-o"></i>
                <span>Financeiro</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Gerenciar</a></li>                
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Contas à Pagar</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Contas à Receber</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Atualizar Base</a></li>                
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Enviar Notificação</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Relatório</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-laptop"></i>
                <span>Suporte Técnico</span>
                <small class="badge pull-right bg-red">3</small>
                <!--<i class="fa fa-angle-left pull-right"></i>-->
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-angle-double-right"></i> 
                    Meus Tickets</a></li>                                    
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-edit"></i> <span>Agenda Técnica</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Meus Compromissos</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right"></i> Eventos</a></li>
              </ul>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-table"></i> <span>Meus Dados</span>
              </a>
            </li>            
          </ul>