@section('HeaderPage')
<h1>
  Adicionar Clientes
  <small>Com tanta facilidade fica muito fácil</small>
</h1>
<ol class="breadcrumb">
  <li><a href="#"><i class="fa fa-dashboard"></i> Clientes</a></li>
  <li class="active">Inserção</li>
</ol>
@stop

@section('Content')
{{ Form::open(array('action' => 'Admin\ClientesController@postStore', 'role' => 'form')) }}

<div class="row" style="margin-bottom: 2em;">
  <div class="col-xs-6">
    <a href="{{ url('empresas') }}" class="btn btn-warning btn-lg btn-flat">Cancelar</a>
    {{ Form::submit('Cadastrar', array('class' => 'btn btn-primary btn-lg btn-flat')) }}
  </div>  
</div>


@if ($errors->any())
<div class="row">
  <div class="callout callout-danger">
    <h4>Você precisa corrigir as questões abaixo:</h4>
    <ul>
      {{ implode('', $errors->all('<li>:message</li>')) }}
    </ul>
  </div>
</div>
@endif

<div class="row">
  <div class="col-xs-12">
    <div class="box box-primary">

      <div class="box-header">        
        <h3 class="box-title">
          Digite os dados do novo Cliente
        </h3>
      </div>

      <div class="box-body">

        <div class="row">
          <div class="col-xs-2">
            <div class="preview" style="float:left; border:1px solid #ccc; padding:2px; width: 154px; height:155px; margin-bottom: 20px">              
              <img src="{{ asset('img/no-image.png') }}" >
            </div>
          </div> 
          <div class="col-xs-3"> 
            {{ Form::label('imagem', 'Seu cliente tem foto?',  array('class' => "text-muted")) }} 
            {{ Form::file('imagem', null, array("class" => "form-control")) }} 
          </div>
        </div>

        <div class="row">

          <div class="col-xs-6"> 
            {{ Form::label('razao_social', 'Razão Social',  array('class' => "text-muted")) }} 
            {{ Form::text('razao_social', null, array("class" => "form-control")) }} 
          </div>

          <div class="col-xs-3"> 
            {{ Form::label('nome_fantasia', 'Nome Fantasia',  array('class' => "text-muted")) }} 
            {{ Form::text('nome_fantasia', null, array("class" => "form-control")) }} 
          </div>

          <div class="col-xs-3"> 
            {{ Form::label('email', 'E-mail',  array('class' => "text-muted")) }} 
            {{ Form::text('email', null, array("class" => "form-control")) }} 
          </div>
        </div>
        <div class="row">
          <div class="col-xs-3"> 

            {{ Form::label('cnpj', 'CNPJ',  array('class' => "text-muted")) }} 
            {{ Form::text('cnpj', null, array("class" => "form-control", 'data-mask' => '', 'data-inputmask' => "'mask': '99.999.999/9999-99'")) }} 
          </div>

          <div class="col-xs-3"> 
            {{ Form::label('inscricao_estadual', 'Insc. Estadual',  array('class' => "text-muted")) }} 
            {{ Form::text('inscricao_estadual', null, array("class" => "form-control", 'data-mask' => '', 'data-inputmask' => "'mask': '999.99999-9'")) }} 
          </div>

          <div class="col-xs-3"> 
            {{ Form::label('inscricao_municipal', 'Insc. Municipal',  array('class' => "text-muted")) }} 
            {{ Form::text('inscricao_municipal', null, array("class" => "form-control")) }} 
          </div>

          <div class="col-xs-3"> 
            {{ Form::label('reg_mec', 'Registro do MEC',  array('class' => "text-muted")) }} 
            {{ Form::text('reg_mec', null, array("class" => "form-control")) }} 
          </div>

        </div>

        <div class="row">

          <div class="col-xs-6"> 
            {{ Form::label('endereco', 'Endereco', array('class' => 'text-muted')) }} 
            {{ Form::text('endereco', null, array("class" => "form-control")) }} 
          </div>

          <div class="col-xs-1"> 
            {{ Form::label('numero_endereco', 'Nº',  array('class' => "text-muted")) }} 
            {{ Form::text('numero_endereco', null, array("class" => "form-control")) }} 
          </div>

          <div class="col-xs-5"> 
            {{ Form::label('bairro', 'Bairro',  array('class' => "text-muted")) }} 
            {{ Form::text('bairro', null, array("class" => "form-control")) }} 
          </div>  

          <div class="col-xs-12"> 
            {{ Form::label('complemento', 'Complemento',  array('class' => "text-muted")) }} 
            {{ Form::text('complemento', null, array("class" => "form-control")) }} 
          </div>  

          <div class="col-xs-6"> 
            {{ Form::label('cidade', 'Cidade',  array('class' => "text-muted")) }} 
            {{ Form::text('cidade', null, array("class" => "form-control")) }} 
          </div>  

          <div class="col-xs-2"> 
            {{ Form::label('uf', 'Uf',  array('class' => "text-muted")) }} 
            {{ Form::text('uf', null, array("class" => "form-control")) }} 
          </div>   

        </div>

        <div class="row">

          <div class="col-xs-3"> 
            {{ Form::label('telefone_empresarial', 'Telefone Empresarial',  array('class' => "text-muted")) }} 
            {{ Form::text('telefone_empresarial', null, array("class" => "form-control", 'data-mask' => '', 'data-inputmask' => "'mask': '(999) 9999-9999'")) }} 
          </div>

          <div class="col-xs-3"> 
            {{ Form::label('responsavel', 'Responsável',  array('class' => "text-muted")) }} 
            {{ Form::text('responsavel', null, array("class" => "form-control")) }} 
          </div>

          <div class="col-xs-3"> 
            {{ Form::label('celular_responsavel', 'Celular Responsável',  array('class' => "text-muted")) }} 
            {{ Form::text('celular_responsavel', null, array("class" => "form-control", 'data-mask' => '', 'data-inputmask' => "'mask': '(999) 9999-9999'")) }} 
          </div>

          <div class="col-xs-3"> 
            {{ Form::label('telefone_responsavel', 'Telefone Responsável',  array('class' => "text-muted")) }} 
            {{ Form::text('telefone_responsavel', null, array("class" => "form-control", 'data-mask' => '', 'data-inputmask' => "'mask': '(999) 9999-9999'")) }} 
          </div>

          <div class="col-xs-3"> 
            {{ Form::label('cpf_responsavel', 'CPF Responsável',  array('class' => "text-muted")) }} 
            {{ Form::text('cpf_responsavel', null, array("class" => "form-control", 'data-mask' => '', 'data-inputmask' => "'mask': '999.999.999-99'")) }} 
          </div>

        </div> 
        <div class="row">

          <div class="col-xs-4"> 
            {{ Form::label('dia_vencimento', 'Dia de Vencimento',  array('class' => "text-muted")) }} 
            {{ Form::select('dia_vencimento', array('10' => 'Dia 10', '15' => 'Dia 15', '20' => 'Dia 20'), '1', array('class' => 'form-control')); }} 
          </div>

          <div class="col-xs-4"> 
            {{ Form::label('forma_pagamento', 'Forma Pagamento',  array('class' => "text-muted")) }} 
            {{ Form::select('forma_pagamento', array('1' => 'Boleto', '2' => 'Débito Automático'), '1', array('class' => 'form-control')); }} 
          </div>

          <div class="col-xs-4"> 
            {{ Form::label('tipo_plano', 'Tipo Plano',  array('class' => "text-muted")) }} 
            {{ Form::select('tipo_plano', array('1' => 'Básico', '2' => 'Avançado'), '1', array('class' => 'form-control')); }} 
          </div>


        </div>

        <div class="row">

          <div class="col-xs-4"> 
            {{ Form::label('host_db', 'Servidor MySql',  array('class' => "text-muted")) }} 
            {{ Form::text('host_db', null, array("class" => "form-control")) }} 
          </div>

          <div class="col-xs-4"> 
            {{ Form::label('base_db', 'Banco de Dados',  array('class' => "text-muted")) }} 
            {{ Form::text('base_db', null, array("class" => "form-control")) }} 
          </div>

          <div class="col-xs-4"> 
            {{ Form::label('user_db', 'Usuário do BD',  array('class' => "text-muted")) }} 
            {{ Form::text('user_db', null, array("class" => "form-control")) }} 
          </div>

          <div class="col-xs-4"> 
            {{ Form::label('password_db', 'Senha do  BD',  array('class' => "text-muted")) }} 
            {{ Form::text('password_db', null, array("class" => "form-control")) }} 
          </div>

          <div class="col-xs-4"> 
            {{ Form::label('port_db', 'Porta do BD',  array('class' => "text-muted")) }} 
            {{ Form::text('port_db', null, array("class" => "form-control")) }} 
          </div>

          <div class="col-xs-4"> 
            {{ Form::label('collation_db', 'Collation do  BD',  array('class' => "text-muted")) }} 
            {{ Form::text('collation_db', null, array("class" => "form-control")) }} 
          </div>

        </div>

        <div class="row">

          <div class="col-xs-4"> 
            {{ Form::label('host_ftp', 'Servidor do FTP',  array('class' => "text-muted")) }} 
            {{ Form::text('host_ftp', null, array("class" => "form-control")) }} 
          </div>

          <div class="col-xs-4"> 
            {{ Form::label('user_ftp', 'Usuário do FTP',  array('class' => "text-muted")) }} 
            {{ Form::text('user_ftp', null, array("class" => "form-control")) }} 
          </div>

          <div class="col-xs-4"> 
            {{ Form::label('password_ftp', 'Password do FTP',  array('class' => "text-muted")) }} 
            {{ Form::text('password_ftp', null, array("class" => "form-control")) }} 
          </div>

          <div class="col-xs-4"> 
            {{ Form::label('port_ftp', 'Porta do FTP',  array('class' => "text-muted")) }} 
            {{ Form::text('port_ftp', null, array("class" => "form-control")) }} 
          </div>
        </div>
        <div class="row">
          <div class="col-xs-4"> 
            {{ Form::label('status', 'Ativar agora esse cliente?',  array('class' => "text-muted")) }} 
            {{ Form::checkbox('status', null, false, array("class" => "form-control")) }} 
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<div class="row">
  <div class="col-xs-12">
    <a href="{{ url('clientes') }}" class="btn btn-warning btn-lg btn-flat">Cancelar</a>
    {{ Form::submit('Cadastrar', array('class' => 'btn btn-primary btn-lg btn-flat')) }}
    {{ Form::close() }}
  </div>

</div>

<script type="text/javascript">
  $(function() {
    //Money Euro
    $("[data-mask]").inputmask();
  });
</script>
@stop