@section('HeaderPage')
<h1>
  Clientes
  <small>Gerencie seus clientes com facilidade</small>
</h1>
<ol class="breadcrumb">
  <li><a href="#"><i class="fa fa-dashboard"></i> Clientes</a></li>
  <li class="active">Listagem</li>
</ol>
@stop


@section('Content')
@if(count($clientes))
<div class="row" style="margin-bottom: 2em;">
  <div class="col-xs-6">
    <a href="{{ url('empresas/create') }}" class="btn btn-success btn-lg btn-flat">Novo Cliente</a>
    {{ Form::open(array('action' => 'Admin\ClientesController@postDestroy')) }}
    {{ Form::hidden('marcados[]', 'marcados', null, array('class' => 'marcados')) }}
    {{ Form::submit('Remover Marcados', array('class' => 'btn btn-warning btn-lg btn-flat')) }}
    {{ Form::close() }}    
  </div>  
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="box box-primary">
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr>
            <th class="checked-all">{{ Form::checkbox('checked-all', '1', false, array("class" => "form-control checked-all")) }}</th>
            <th>#</th>
            <th>Cliente</th>
            <th>CNPJ</th>
            <th>Telefone</th>
            <th>Status</th>
            <th></th>
          </tr>
          @foreach($clientes as $cliente)
          <tr>
            <td>{{ Form::checkbox('data-id[]', $cliente->id, false, array("class" => "form-control", 'data-id' => $cliente->id)) }}</td>
            <td>{{ $cliente->id }}</td>
            <td><a href="{{ url('empresas/show', array('id' => $cliente->id)) }}">{{ $cliente->razao_social }}</a></td>
            <td>{{ $cliente->cnpj }}</td>
            <td>{{ $cliente->telefone_empresarial }}</td>
            <td><span class="label label-{{ ($cliente->status == '1') ? 'success' : 'warning'}}">{{ ($cliente->status == '1') ? 'Ativo' : 'Desativado'}}</span></td>
            <td><a href='{{ url('empresas/show', array('id' => $cliente->id)) }}' class="btn btn-info btn-flat"><i class="fa fa-edit"></i></a></td>
          </tr>          
          @endforeach
        </table>
        @else
        <h1>Não existem empresas cadastradas.</h1>
        @endif
      </div><!-- /.box-body --> 
      <div class="box-footer clearfix">
        <div class="col-xs-6">
          <p class='text-muted'>Exibindo <strong>{{ count($clientes) }}</strong> de <strong>{{ count($clientes) }}</strong> registros</p>
        </div>
        <div class="col-xs-6">
          {{ $clientes->links() }}
        </div>
      </div>
    </div><!-- /.box -->
  </div>
</div>

@stop