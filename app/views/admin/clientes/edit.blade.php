@extends('admin.layouts.default')

{{-- Content --}}
@section('content')


<h1>Editar Cliente</h1>

    @if ($errors->any())
        <ul>
            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
        </ul>
    @endif

    {{ Form::model($cliente, array('method' => 'PATCH', 'route' => array('clientes.update', $cliente->id))) }}
        <div class="form-group"> 
            {{ Form::label('razao_social', 'Razao social',  array('class' => "control_label")) }} 
            {{ Form::text('razao_social', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('nome_fantasia', 'Nome fantasia',  array('class' => "control_label")) }} 
            {{ Form::text('nome_fantasia', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('email', 'Email',  array('class' => "control_label")) }} 
            {{ Form::text('email', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('cnpj', 'Cnpj',  array('class' => "control_label")) }} 
            {{ Form::text('cnpj', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('inscricao_estadual', 'Inscricao estadual',  array('class' => "control_label")) }} 
            {{ Form::text('inscricao_estadual', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('imagem', 'Imagem',  array('class' => "control_label")) }} 
            {{ Form::text('imagem', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('endereco', 'Endereco',  array('class' => "control_label")) }} 
            {{ Form::text('endereco', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('numero_endereco', 'Numero endereco',  array('class' => "control_label")) }} 
            {{ Form::text('numero_endereco', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('bairro', 'Bairro',  array('class' => "control_label")) }} 
            {{ Form::text('bairro', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('complemento', 'Complemento',  array('class' => "control_label")) }} 
            {{ Form::text('complemento', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('cidade', 'Cidade',  array('class' => "control_label")) }} 
            {{ Form::text('cidade', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('uf', 'Uf',  array('class' => "control_label")) }} 
            {{ Form::text('uf', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('telefone_empresarial', 'Telefone empresarial',  array('class' => "control_label")) }} 
            {{ Form::text('telefone_empresarial', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('responsavel', 'Responsavel',  array('class' => "control_label")) }} 
            {{ Form::text('responsavel', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('celular_responsavel', 'Celular responsavel',  array('class' => "control_label")) }} 
            {{ Form::text('celular_responsavel', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('telefone_responsavel', 'Telefone responsavel',  array('class' => "control_label")) }} 
            {{ Form::text('telefone_responsavel', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('cpf_responsavel', 'Cpf responsavel',  array('class' => "control_label")) }} 
            {{ Form::text('cpf_responsavel', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('dia_vencimento', 'Dia vencimento',  array('class' => "control_label")) }} 
            {{ Form::text('dia_vencimento', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('forma_pagamento', 'Forma pagamento',  array('class' => "control_label")) }} 
            {{ Form::text('forma_pagamento', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('tipo_plano', 'Tipo plano',  array('class' => "control_label")) }} 
            {{ Form::text('tipo_plano', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('host_db', 'Host db',  array('class' => "control_label")) }} 
            {{ Form::text('host_db', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('base_db', 'Base db',  array('class' => "control_label")) }} 
            {{ Form::text('base_db', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('user_db', 'User db',  array('class' => "control_label")) }} 
            {{ Form::text('user_db', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('password_db', 'Password db',  array('class' => "control_label")) }} 
            {{ Form::text('password_db', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('port_db', 'Port db',  array('class' => "control_label")) }} 
            {{ Form::text('port_db', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('collation_db', 'Collation db',  array('class' => "control_label")) }} 
            {{ Form::text('collation_db', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('host_ftp', 'Host ftp',  array('class' => "control_label")) }} 
            {{ Form::text('host_ftp', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('user_ftp', 'User ftp',  array('class' => "control_label")) }} 
            {{ Form::text('user_ftp', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('password_ftp', 'Password ftp',  array('class' => "control_label")) }} 
            {{ Form::text('password_ftp', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('port_ftp', 'Port ftp',  array('class' => "control_label")) }} 
            {{ Form::text('port_ftp', null, array("class" => "form-control")) }} 
        </div>

        <div class="form-group"> 
            {{ Form::label('status', 'Status',  array('class' => "control_label")) }} 
            {{ Form::text('status', null, array("class" => "form-control")) }} 
        </div>
        <div class="form-group"> 
        	{{ Form::submit('Update', array('class' => 'btn btn-info')) }}
        	{{ link_to_route('clientes.show', 'Cancel', $cliente->id, array('class' => 'btn')) }}
        </div>
    {{ Form::close() }}



@stop
