{{ Form::open(array('action' => 'UsersController@store', 'method' => 'post')) }}

  {{ Form::label('nome', 'Nome', array('class' => "label")) }}
  {{ Form::text('nome', '', array("class" => "input")) }}
  
  {{ Form::label('sobrenome', 'Sobrenome', array('class' => "label")) }}
  {{ Form::text('sobrenome', '', array("class" => "input")) }}

  {{ Form::label('email', 'E-Mail', array('class' => "label")) }}
  {{ Form::text('email', '', array("class" => "input")) }}
    
  {{ Form::label('password', 'Password', array('class' => "label")) }}
  {{ Form::password('password', '', array("class" => "input")) }}

  {{ Form::label('status', 'Marque para ativar o usuário', array('class' => "label")) }}
  {{ Form::checkbox('status', '1') }}

  {{ Form::submit('Cadastrar') }}

{{ Form::close() }}
