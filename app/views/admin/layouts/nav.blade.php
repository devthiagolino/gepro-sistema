<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a href="#" class="navbar-brand">GEPRO - GERENCIADOR</a>
	</div>

	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">

		
			<ul class="nav navbar-nav side-nav">
				<li><a href="#"><i class="fa fa-home"></i> Página Inicial</a>
				</li>
				
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i> Administração <b
							class="caret"></b></a>
					<ul class="dropdown-menu">
						<!-- usuários -->
						<li class="dropdown">
							<a href="#" >
								<i class="fa fa-search"></i> Usuários 
							</a>
						</li>

						<!-- Trabalhos Registrados -->
						<li class="dropdown">
							<a href="#" >
								<i class="fa fa-edit"></i> Trabalhos 
							</a>
						</li>

						<!-- Dependentes -->
						<li class="dropdown">
							<a href="#" >
								<i class="fa fa-users"></i> Dependentes 
							</a>
						</li>

						<!-- Dados Funcionário -->
						<li class="dropdown">
							<a href="#" >
								<i class="fa fa-search"></i> Dados Funcionários 
							</a>
						</li>
						
					</ul>
				</li>
<!--				<li><a href="tables.html"><i class="fa fa-list"></i> Activity</a></li>-->
			</ul>



		@if (Auth::check())

		<ul class="nav navbar-nav navbar-right navbar-user">
			<li class="dropdown user-dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>{{{ Auth::user()->username }}} <b
						class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
					<li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
					<li class="divider"></li>
					<li><a href="#"><i class="fa fa-power-off"></i> Log Out</a></li>
				</ul>
			</li>
		</ul>
		@endif
	</div>
	<!-- /.navbar-collapse -->
</nav>