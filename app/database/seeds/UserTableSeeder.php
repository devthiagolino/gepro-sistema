<?php

class UserTableSeeder extends Seeder {

  public function run() {


    // Create the user
    Sentry::createUser(array(
        'email' => 'hyago@gepro.com.br',
        'password' => '123',
        'activated' => true,
        'permissions' => array(
            'user.create' => -1,
            'user.delete' => -1,
            'user.view' => 1,
            'user.update' => 1,
        )
    ));

    //User::create(array('email' => 'foo@bar.com'));
  }

}
