<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clientes', function(Blueprint $table)
		{
			$table->increments("id");
                        $table->string("razao_social", 255);
                        $table->string("nome_fantasia", 255);                        
                        $table->string("email", 255)->unique();    
                        $table->integer("cnpj");
                        $table->integer("inscricao_estadual");
                        $table->string("imagem", 4);
                        
                        $table->string("endereco", 255);
                        $table->integer("numero_endereco");
                        $table->string("bairro", 255);
                        $table->string("complemento", 255);
                        $table->string("cidade", 255);
                        $table->string("uf", 2);
                        
                        $table->integer("telefone_empresarial");
                        
                        $table->string("responsavel", 255);
                        $table->integer("celular_responsavel");
                        $table->integer("telefone_responsavel");
                        $table->integer("cpf_responsavel");
                        
                        $table->integer("dia_vencimento");
                        $table->string("forma_pagamento");
                        $table->string("tipo_plano", 1);

                        $table->string("host_db", 100);
                        $table->string("base_db", 100);
                        $table->string("user_db", 100);
                        $table->string("password_db", 100);
                        $table->string("port_db", 10);
                        $table->string("collation_db", 10);
                        
                        $table->string("host_ftp", 100);
                        $table->string("user_ftp", 100);
                        $table->string("password_ftp", 100);
                        $table->string("port_ftp", 10);
                        
                        $table->string("status", 1);
                        
                        $table->timestamps();
                        $table->softDeletes();
                        
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('clientes', function(Blueprint $table)
		{
			//
		});
	}

}