<?php

namespace Admin;
use \Eloquent;

class Cliente extends \Eloquent {

  protected $fillable = [];
  protected $guarded = array('id');
  protected $table = 'clientes';
  
  public static $rules = array(
      'email' => 'required',
      'cnpj' => 'required'
  );
  
  
  public function mostrarFotoCliente(){
    return 'clientes/'.$this->id.'_empresa.'.$this->imagem;
  }

}
